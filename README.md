# Pa11y CI Reporter Runner

Pa11y CI Reporter Runner facilitates testing of
[Pa11y CI reporters](https://github.com/pa11y/pa11y-ci#write-a-custom-reporter).
Given a Pa11y CI JSON results file and optional configuration it performs the
Pa11y CI calls to the reporter, including proper transformation of results and
configuration data. Functionally, it's an emulation of the Pa11y CI side of the
reporter interface with explicit control over execution of each step.

## Installation

Install Pa11y CI Reporter Runner via
[npm](https://www.npmjs.com/package/pa11y-ci-reporter-runner).

```sh
npm install pa11y-ci-reporter-runner
```

## Usage

Pa11y CI Reporter Runner exports a factory function `createRunner` that creates
a reporter runner. This function takes four arguments:

- `resultsFileName`: Path to a Pa11y CI JSON results file.
- `reporterName`: Name of the reporter to execute. Can be an npm module, for
  example `pa11y-ci-reporter-html`, or a path to a reporter file.
- `options`: Optional
  [reporter options](https://github.com/pa11y/pa11y-ci#reporter-options).
- `config`: Optional Pa11y CI configuration file that produces the Pa11y CI
  JSON results.

### Runner states

Pa11y CI Reporter Runner emulates calls from Pa11y CI to the given reporter for
the given results file. There are five distinct states for the runner, as shown
below:

```mermaid
%% It's unfortunate that mermaid charts don't render on npmjs.com,
%% see the README in the repository to view the flowchart
flowchart LR;
    init-->beforeAll;
    beforeAll-->beginUrl;
    beginUrl-->urlResults;
    urlResults-->beginUrl;
    urlResults-->afterAll;
```

- `init`: The initial state of the runner.
- `beforeAll`: In this state the runner calls the reporter `beforeAll`
  function.
- `beginUrl`: In this state the runner calls the reporter `begin` function for
  a given URL.
- `urlResults`: In this state the runner calls the reporter `results` or
  `error` function depending on the result for that URL. If there are
  subsequent URLs, the runner next moves to `beginUrl` for the next URL. If
  this is the last URL, the runner moves to `afterAll`.
- `afterAll`: In this state the runner calls the reporter `afterAll` function.

When calling reporter functions, the runner transforms the Pa11y CI results and
configuration data to provide the appropriate arguments. For example, the
`results` function is called with the results as returned from Pa11y (slightly
different than those returned from Pa11y CI) and the consolidated configuration
for the analysis of that URL.

The runner state can be obtained via the following runner functions:

- `getCurrentState()`: The current state of the runner.
- `getNextState()`: The next state of the runner (that is, the state that would
  be obtained by calling the `runNext()` function).

Both functions return an object with the following properties:

- `state`: The current runner state (any state value shown previously)
- `url`: The current URL for any state with an applicable URL (`beginUrl` and
  `urlResults`), otherwise `undefined`.

### Runner execution

The reporter runner has five control functions:

- `runAll()`: Simulates Pa11y CI running the analysis from the provided JSON
  results file from the current state through the end, calling all associated
  reporter functions.
- `runNext()`: Simulates Pa11y CI running through the next state from the
  provided JSON results file, calling the associated reporter function as noted
  previously.
- `runUntil(targetState, targetUrl)`: Simulates Pa11y CI running the analysis
  from the provided JSON results file from the current state through the
  specified state/URL, calling the associated reporter functions as noted
  previously. An error is thrown if the end of the results are reached and the
  target wasn't found. This function takes the following arguments:
  - `targetState`: The target state of the runner (any state noted previously
    except `init`, or any valid value of the `RunnerStates` enum).
  - `targetUrl`: An optional target URL. If no URL is specified, the runner
    stops at the first instance of the target state.
- `runUntilNext(targetState, targetUrl)`: Provides the same capability as
  `runUntil`, but execution ends at the state prior to the target state/URL,
  so that the target would execute if `runNext()` is subsequently called.
- `reset()`: Resets the runner to the `init` state and re-initializes the
  reporter. This can be sent from any state.

These command are all asynchronous and must be completed before another is
sent, otherwise an error is thrown. In addition, once a run has been completed
and the runner is in the `afterAll` state it must be `reset` before accepting
any run command.

### Example

A complete example is provided below:

```js
const { createRunner, RunnerStates } = require('pa11y-ci-reporter-runner');

const resultsFileName = 'pa11yci-results.json';
const reporterName = '../test-reporter.js';
const reporterOptions = { isSomething: true };
const config = {
  defaults: {
    timeout: 30000
  },
  urls: [
    'http://localhost:8080/page1-with-errors.html',
    'http://localhost:8080/page1-no-errors.html',
    {
      url: 'https://pa11y.org/timed-out.html',
      timeout: 50
    }
  ]
};

test('test all reporter functions', async () => {
  const runner = createRunner(
    resultsFileName,
    reporterName,
    reporterOptions,
    config
  );

  await runner.runAll();

  // Test reporter results
});

test('test reporter at urlResults state', async () => {
  const runner = createRunner(
    resultsFileName,
    reporterName,
    reporterOptions,
    config
  );

  await runner.runUntil(
    RunnerStates.beginUrl,
    'http://localhost:8080/page1-no-errors.html'
  );
  let currentState = runner.getCurrentState();
  // { state: "beginUrl", url: "http://localhost:8080/page1-no-errors.html" }
  const nextState = runner.getNextState();
  // { state: "urlResults", url: "http://localhost:8080/page1-no-errors.html" }
  await runner.runNext();
  currentState = runner.getCurrentState();
  // { state: "urlResults", url: "http://localhost:8080/page1-no-errors.html" }

  // Test reporter results
});
```

## Limitations

When passing config to `results`, `error`, and `afterAll`, Pa11y CI Reporter
Runner includes the same properties as Pa11y CI except the `browser` property
(with the `puppeteer` `browser` object used by Pa11y CI). If the `browser`
object is needed, testing should be done with Pa11y CI to ensure proper
`browser` capabilities are available.
