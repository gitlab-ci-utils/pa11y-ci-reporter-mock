import { expectError, expectType } from 'tsd';
import { createRunner, Runner, RunnerState } from '.';

// Test all functions with valid types
const runner = createRunner('results.json', 'pa11y-ci-reporter-html');
runner.runNext();
runner.runUntil('beginUrl', 'https://example.test');
const currentState = runner.getCurrentState();
const nextState = runner.getNextState();
runner.reset();
runner.runUntilNext('beginUrl');
runner.runAll();

createRunner(
  'results.json',
  'pa11y-ci-reporter-html',
  { foo: 'bar' },
  { urls: ['https://example.test'] }
);

createRunner('results.json', 'pa11y-ci-reporter-html', {}, {});

// Test return types
expectType<Runner>(runner);
expectType<RunnerState>(currentState);
expectType<RunnerState>(nextState);

// Test all functions with invalid types
expectError(createRunner());
expectError(createRunner('results.json', 'pa11y-ci-reporter-html', 'invalid'));
expectError(runner.runNext('invalid'));
expectError(runner.runUntil('invalid', 'https://example.test'));
expectError(runner.runUntilNext('invalid'));
expectError(runner.reset('invalid'));
expectError(runner.getCurrentState('invalid'));
expectError(runner.getNextState('invalid'));
expectError(runner.runAll('invalid'));
