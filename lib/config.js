'use strict';

/**
 * Manages Pa11y CI configuration.
 *
 * @module config
 */

const { defaultsDeep, omit } = require('lodash');

const defaultConfig = require('./default-config');

const normalizeConfig = (config) => {
    // Remove log and reporters to be consistent with pa11y-ci
    const configDefaults = omit(config.defaults || {}, ['log', 'reporters']);
    return {
        defaults: defaultsDeep({}, configDefaults, defaultConfig),
        urls: config.urls || []
    };
};

/**
 * Factory function that returns a config object for managing Pa11y CI
 * configuration including defaults and determining the consolidated
 * configuration for each URL.
 *
 * @param   {object} config The Pa11y CI configuration.
 * @returns {object}        Config object.
 * @static
 */
const configFactory = (config) => {
    const { defaults, urls } = normalizeConfig(config);

    const getConfigForUrl = (url) => {
        if (!urls || urls.length === 0) {
            return defaults;
        }

        // Config URLs are validated against results, if specified, so will find a result.
        const result = urls.find(
            (urlObject) => urlObject === url || urlObject.url === url
        );
        if (typeof result === 'string') {
            return defaults;
        }

        return defaultsDeep({}, result, defaults);
    };

    return {
        defaults,
        getConfigForUrl
    };
};

module.exports = configFactory;
