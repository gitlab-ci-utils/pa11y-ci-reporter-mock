'use strict';

/**
 * Default pa11y-ci configuration.
 *
 * @module default-config
 */

const defaultColumnWidth = 80;

const defaultConfig = {
    concurrency: 1,
    useIncognitoBrowserContext: true,
    wrapWidth: process.stdout.columns || defaultColumnWidth
};

module.exports = defaultConfig;
