'use strict';

/**
 * Reformats pa11y-ci JSON data.
 *
 * @module formatter
 */

const isError = (issues) =>
    issues.length === 1 &&
    Object.keys(issues[0]).length === 1 &&
    issues[0].message;

/**
 * Converts Pa11y CI JSON output to an equivalent Pa11y CI object,
 * allowing JSON result files to be used for reporter interface
 * testing. Error messages are converted to Error objects.
 *
 * @param   {object} jsonResults Pa11y CI JSON results.
 * @returns {object}             The equivalent Pa11y CI object.
 * @static
 */
const convertJsonToResultsObject = (jsonResults) => {
    const results = {
        errors: jsonResults.errors,
        passes: jsonResults.passes,
        results: {},
        total: jsonResults.total
    };

    for (const url of Object.keys(jsonResults.results)) {
        // User supplied input used to index user supplied data file
        // nosemgrep: eslint.detect-object-injection
        const issues = jsonResults.results[url];
        const formattedIssues = isError(issues)
            ? [new Error(issues[0].message)]
            : issues;
        // User supplied input used as object key
        // nosemgrep: eslint.detect-object-injection
        results.results[url] = formattedIssues;
    }

    return results;
};

/**
 * Checks the given Pa11y CI results for the given URL, and if found
 * returns a Pa11y results object of the format sent to the reporter
 * results event. Throws if the url is not found in the results.
 *
 * @param   {string} url     The URL to find results for,.
 * @param   {object} results Pa11y CI results object.
 * @returns {object}         The Pa11y results object for the URL.
 * @throws  {Error}          Throws if results are not found for the given URL.
 * @static
 */
const getPa11yResultsFromPa11yCiResults = (url, results) => {
    // User supplied input used to index user supplied data file
    // nosemgrep: eslint.detect-object-injection
    const issues = results.results[url];
    if (!issues) {
        throw new Error(`Results for url "${url} not found`);
    }

    return {
        documentTitle: `Title for ${url}`,
        issues,
        pageUrl: url
    };
};

module.exports.convertJsonToResultsObject = convertJsonToResultsObject;
module.exports.getPa11yResultsFromPa11yCiResults =
    getPa11yResultsFromPa11yCiResults;
