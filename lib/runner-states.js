'use strict';

/**
 * Valid states for the runner.
 *
 * @module runner-states
 */

/**
 * Enum for runner states.
 *
 * @readonly
 * @enum {string}
 * @static
 */
const RunnerStates = Object.freeze({
    afterAll: 'afterAll',
    beforeAll: 'beforeAll',
    beginUrl: 'beginUrl',
    urlResults: 'urlResults'
});

module.exports = RunnerStates;
