'use strict';

const urlNoIssues = 'somewhere-no-issues.html';
const urlWithIssues = 'somewhere-with-issues.html';
const urlError = 'somewhere-error.html';

const invalidConfigInvalidUrl = {
    defaults: {
        timeout: 30_000
    },
    urls: [
        urlWithIssues,
        urlNoIssues,
        {
            timeout: 50
        }
    ]
};

const invalidConfigMissingUrl = {
    defaults: {
        timeout: 30_000
    },
    urls: [
        urlWithIssues,
        {
            timeout: 50,
            url: urlError
        }
    ]
};

const invalidConfigWrongUrls = {
    defaults: {
        timeout: 30_000
    },
    urls: [
        `./some/path/${urlNoIssues}`,
        urlWithIssues,
        {
            timeout: 50,
            url: urlError
        }
    ]
};

const validConfigAllStringUrl = {
    defaults: {
        timeout: 30_000
    },
    urls: [urlNoIssues, urlWithIssues, urlError]
};

const validConfigNoUrls = {
    defaults: {
        timeout: 30_000
    }
};

const validConfigStringObjectUrl = {
    defaults: {
        timeout: 30_000,
        viewport: {
            height: 768,
            width: 1024
        },
        wrapWidth: 123
    },
    urls: [
        urlNoIssues,
        {
            url: urlWithIssues
        },
        {
            timeout: 50,
            url: urlError,
            viewport: {
                isMobile: true
            }
        }
    ]
};

module.exports.invalidConfigInvalidUrl = invalidConfigInvalidUrl;
module.exports.invalidConfigMissingUrl = invalidConfigMissingUrl;
module.exports.invalidConfigWrongUrls = invalidConfigWrongUrls;
module.exports.validConfigAllStringUrl = validConfigAllStringUrl;
module.exports.validConfigNoUrls = validConfigNoUrls;
module.exports.validConfigStringObjectUrl = validConfigStringObjectUrl;
