'use strict';

/**
 * Pa11y CI reporter that counts function calls.
 *
 * @module counter-reporter
 */

/**
 * Pa11y CI reporter that counts function calls. Used to
 * test state management and resetting reporter.
 *
 * @returns {object} Pa11y CI counter reporter.
 * @public
 */
const reporter = () => {
    let counter = 0;
    // All reporter interface functions simply increment the counter and log to console
    return {
        /* eslint-disable sort-keys -- execution sequence */
        beforeAll: () => {
            console.log(++counter);
        },
        begin: () => {
            console.log(++counter);
        },
        results: () => {
            console.log(++counter);
        },
        error: () => {
            console.log(++counter);
        },
        afterAll: () => {
            console.log(++counter);
        }
        /* eslint-enable sort-keys -- execution sequence */
    };
};

module.exports = reporter;
