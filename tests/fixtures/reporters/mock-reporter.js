'use strict';

/**
 * Mock reporter uses jest mocks for all reporter functions.
 *
 * @module mock-reporter
 */
module.exports = {
    afterAll: jest.fn(),
    beforeAll: jest.fn(),
    begin: jest.fn(),
    error: jest.fn(),
    results: jest.fn()
};
