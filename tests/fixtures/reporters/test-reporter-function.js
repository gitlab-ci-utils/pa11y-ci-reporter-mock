'use strict';

/**
 * Reporter that returns a factory function that creates a reporter
 * that implements all reporter functions for testing.
 *
 * @module test-reporter-function
 */

/**
 * Count properties in an object recursively.
 *
 * @param   {object} value The object to check for properties.
 * @returns {number}       The total count of properties.
 * @private
 */
const getPropertyCount = (value) => {
    let count = 0;
    for (const key of Object.keys(value)) {
        count++;
        if (typeof value[key] === 'object') {
            count += getPropertyCount(value[key]);
        }
    }
    return count;
};

/**
 * Pa11y CI reporter that implements all reporter functions for testing.
 * Returns a reporter factory function. Logs the function and applicable
 * arguments to stdout.
 *
 * @param   {object} options Reporter options object.
 * @param   {object} config  Pa11y CI config.
 * @returns {object}         Pa11y CI test reporter.
 * @public
 */
// eslint-disable-next-line max-lines-per-function -- factory function with state
const reporter = (options, config) => ({
    /* eslint-disable sort-keys -- execution sequence */
    beforeAll: (urls) => {
        console.log(
            `beforeAll - ${urls.length} urls, ${getPropertyCount(
                options
            )} option settings,` +
                ` ${getPropertyCount(config)} config settings`
        );
    },

    begin: (url) => {
        console.log(`begin - ${url}`);
    },

    // eslint-disable-next-line no-shadow -- using API nomenclature
    results: (results, config) => {
        console.log(
            `results - ${results.pageUrl} - ${
                results.issues.length
            } issues, ${getPropertyCount(config)} config settings`
        );
    },

    // eslint-disable-next-line no-shadow -- using API nomenclature
    error: (error, url, config) => {
        console.log(
            `error - ${url} - ${error.message}, ${getPropertyCount(
                config
            )} config settings`
        );
    },

    // eslint-disable-next-line no-shadow -- using API nomenclature
    afterAll: (report, config) => {
        console.log(
            `afterAll - ${
                Object.keys(report.results).length
            } urls, ${getPropertyCount(config)} config settings`
        );
    }
    /* eslint-enable sort-keys -- execution sequence */
});

module.exports = reporter;
