'use strict';

const formatter = require('../lib/formatter');
const jsonResults = require('./fixtures/results/pa11yci-results-valid.json');

describe('convertJsonToResultsObject', () => {
    it('should throw if input has no results', () => {
        expect.assertions(1);
        const results = { errors: 2, passes: 1, total: 3 };

        expect(() => formatter.convertJsonToResultsObject(results)).toThrow(
            'Cannot convert undefined'
        );
    });

    describe('results object', () => {
        let resultsObject;

        beforeAll(() => {
            resultsObject = formatter.convertJsonToResultsObject(jsonResults);
        });

        it('should contain total property', () => {
            expect.assertions(1);
            expect(typeof resultsObject.total).toBe('number');
        });

        it('should contain passes property', () => {
            expect.assertions(1);
            expect(typeof resultsObject.passes).toBe('number');
        });

        it('should contain errors property', () => {
            expect.assertions(1);
            expect(typeof resultsObject.errors).toBe('number');
        });

        it('should contain results property', () => {
            expect.assertions(1);
            expect(typeof resultsObject.results).toBe('object');
        });

        it('should return original result if not error', () => {
            expect.assertions(1);
            const url = 'somewhere-with-issues.html';
            expect(resultsObject.results[url]).toStrictEqual(
                jsonResults.results[url]
            );
        });

        it('should return array with Error object from message if error', () => {
            expect.assertions(3);
            const url = 'somewhere-error.html';
            expect(resultsObject.results[url]).toHaveLength(1);
            expect(resultsObject.results[url][0] instanceof Error).toBe(true);
            expect(resultsObject.results[url][0].message).toBe(
                jsonResults.results[url][0].message
            );
        });
    });
});

describe('getPa11yResultsFromPa11yCiResults', () => {
    let resultsObject;

    beforeAll(() => {
        resultsObject = formatter.convertJsonToResultsObject(jsonResults);
    });

    it('should throw if url is not found in results', () => {
        expect.assertions(1);
        const url = 'foo.html';
        expect(() =>
            formatter.getPa11yResultsFromPa11yCiResults(url, resultsObject)
        ).toThrow(url);
    });

    describe('results object', () => {
        const url = 'somewhere-with-issues.html';
        let pa11yResults;

        beforeAll(() => {
            pa11yResults = formatter.getPa11yResultsFromPa11yCiResults(
                url,
                resultsObject
            );
        });

        it('should contain documentTitle property derived from url', () => {
            expect.assertions(2);
            expect(typeof pa11yResults.documentTitle).toBe('string');
            expect(pa11yResults.documentTitle).toMatch(url);
        });

        it('should contain pageUrl property that equals url', () => {
            expect.assertions(2);
            expect(typeof pa11yResults.pageUrl).toBe('string');
            expect(pa11yResults.pageUrl).toMatch(url);
        });

        it('should contain issues property that equals results', () => {
            expect.assertions(1);
            expect(pa11yResults.issues).toStrictEqual(
                resultsObject.results[url]
            );
        });
    });
});
