'use strict';

const { defaultsDeep } = require('lodash');
const { createRunner } = require('../index');
const reporterBuilder = require('../lib/reporter-builder');
const defaultConfig = require('../lib/default-config');
const RunnerStates = require('../lib/runner-states');

const defaultReporter = './tests/fixtures/reporters/test-reporter-function.js';
const defaultReporterOptions = { foo: 'bar' };
const defaultReporterResults = require('./fixtures/results/test-reporter-function-results.json');

const echoReporter = './tests/fixtures/reporters/echo-reporter.js';
const echoReporterOptions = { logConsole: true };
const echoReporterResults = require('./fixtures/results/echo-reporter-results-pa11yci.json');

const mockReporter = './tests/fixtures/reporters/mock-reporter.js';
const counterReporter = './tests/fixtures/reporters/counter-reporter.js';

const pa11yciResultsValid =
    './tests/fixtures/results/pa11yci-results-valid.json';
const pa11yciResultsInvalidJson =
    './tests/fixtures/results/pa11yci-results-invalid-json.json';
const pa11yciResultsInvalidResults =
    './tests/fixtures/results/pa11yci-results-invalid-results.json';
const pa11yciResultsNoUrls =
    './tests/fixtures/results/pa11yci-results-no-urls.json';
const pa11yciResults = require('./fixtures/results/pa11yci-results-valid.json');

const {
    invalidConfigInvalidUrl,
    invalidConfigMissingUrl,
    invalidConfigWrongUrls,
    validConfigAllStringUrl,
    validConfigNoUrls,
    validConfigStringObjectUrl
} = require('./fixtures/configs/pa11yci-configs');

const validConfigStringObjectUrlDefaults = defaultsDeep(
    {},
    validConfigStringObjectUrl.defaults,
    defaultConfig
);

const noop = () => {};

describe('reporter runner factory', () => {
    it('should export a factory function that returns a reporter runner', () => {
        expect.assertions(1);
        expect(typeof createRunner).toBe('function');
    });

    describe('reporter runner', () => {
        afterEach(() => {
            jest.restoreAllMocks();
        });

        it('should load the selected reporter when creating runner', () => {
            expect.assertions(1);
            const buildReporterSpy = jest.spyOn(
                reporterBuilder,
                'buildReporter'
            );
            const reporterName = defaultReporter;
            createRunner(
                pa11yciResultsValid,
                reporterName,
                defaultReporterOptions,
                validConfigStringObjectUrl
            );
            expect(buildReporterSpy).toHaveBeenCalledWith(
                reporterName,
                defaultReporterOptions,
                validConfigStringObjectUrlDefaults
            );
        });

        describe('runner control functions', () => {
            let runner;

            beforeAll(() => {
                runner = createRunner(pa11yciResultsValid, defaultReporter);
            });

            const runnerFunctions = [
                { functionName: 'runAll' },
                { functionName: 'runNext' },
                { functionName: 'runUntil' },
                { functionName: 'reset' },
                { functionName: 'getCurrentState' },
                { functionName: 'getNextState' }
            ];

            it.each(runnerFunctions)(
                'should provide a $functionName function',
                ({ functionName }) => {
                    expect.assertions(1);
                    expect(typeof runner[functionName]).toBe('function');
                }
            );
        });

        describe('pa11y-ci results file', () => {
            const fileLoadError = 'Error loading results file';

            it('should throw if fileName is not a string', () => {
                expect.assertions(1);
                const filename = {};
                expect(() => createRunner(filename, mockReporter)).toThrow(
                    'fileName must be a string'
                );
            });

            it('should throw if file cannot be opened', () => {
                expect.assertions(1);
                const filename = './reporter-does-not-exist.json';
                expect(() => createRunner(filename, mockReporter)).toThrow(
                    fileLoadError
                );
            });

            it('should throw if file is invalid JSON', () => {
                expect.assertions(1);
                const filename = pa11yciResultsInvalidJson;
                expect(() => createRunner(filename, mockReporter)).toThrow(
                    fileLoadError
                );
            });

            it('should throw if file is invalid pa11y-ci results', () => {
                expect.assertions(1);
                const filename = pa11yciResultsInvalidResults;
                expect(() => createRunner(filename, mockReporter)).toThrow(
                    fileLoadError
                );
            });
        });

        describe('pa11y ci config', () => {
            const urlError =
                'config.urls is specified and does not match results';

            it('should throw if config url count does not match results urls count', () => {
                expect.assertions(1);
                const config = invalidConfigMissingUrl;
                expect(() =>
                    createRunner(
                        pa11yciResultsValid,
                        mockReporter,
                        defaultReporterOptions,
                        config
                    )
                ).toThrow(urlError);
            });

            it('should throw if config url is invalid', () => {
                expect.assertions(1);
                const config = invalidConfigInvalidUrl;
                expect(() =>
                    createRunner(
                        pa11yciResultsValid,
                        mockReporter,
                        defaultReporterOptions,
                        config
                    )
                ).toThrow('invalid url element');
            });

            it('should throw if config urls do not match results urls', () => {
                expect.assertions(1);
                const config = invalidConfigWrongUrls;
                expect(() =>
                    createRunner(
                        pa11yciResultsValid,
                        mockReporter,
                        defaultReporterOptions,
                        config
                    )
                ).toThrow(urlError);
            });

            it('should not throw if config urls is not specified', () => {
                expect.assertions(1);
                const config = validConfigNoUrls;
                expect(() =>
                    createRunner(
                        pa11yciResultsValid,
                        mockReporter,
                        defaultReporterOptions,
                        config
                    )
                ).not.toThrow();
            });

            it('should not throw if config urls match results urls with all strings', () => {
                expect.assertions(1);
                const config = validConfigAllStringUrl;
                expect(() =>
                    createRunner(
                        pa11yciResultsValid,
                        mockReporter,
                        defaultReporterOptions,
                        config
                    )
                ).not.toThrow();
            });

            it('should not throw if config urls match results urls with strings and objects', () => {
                expect.assertions(1);
                const config = validConfigStringObjectUrl;
                expect(() =>
                    createRunner(
                        pa11yciResultsValid,
                        mockReporter,
                        defaultReporterOptions,
                        config
                    )
                ).not.toThrow();
            });
        });

        describe('reporter interface functions', () => {
            let consoleSpy;

            // Some reporters return JSON and others plain text, so
            // try parsing JSON and if it fails return plain text
            const parseTestResults = (value) => {
                try {
                    return JSON.parse(value);
                } catch {
                    return value;
                }
            };

            // Execute interface test on both synchronous and asynchronous reporters
            const interfaceFunctionTestCases = [
                {
                    label: '(sync)',
                    reporter: defaultReporter,
                    reporterResults: defaultReporterResults
                },
                {
                    label: '(async)',
                    reporter: echoReporter,
                    reporterResults: echoReporterResults
                }
            ];

            describe.each(interfaceFunctionTestCases)(
                'with config $label',

                ({ reporter, reporterResults }) => {
                    beforeEach(async () => {
                        consoleSpy = jest
                            .spyOn(console, 'log')
                            .mockImplementation(noop);

                        // Use runAll to execute all interface function calls
                        const runner = createRunner(
                            pa11yciResultsValid,
                            reporter,
                            echoReporterOptions,
                            validConfigStringObjectUrl
                        );
                        await runner.runAll();
                    });

                    it('should call beforeAll with urls from config if provided', () => {
                        expect.assertions(1);
                        const results = parseTestResults(
                            consoleSpy.mock.calls[0][0]
                        );
                        expect(results).toStrictEqual(
                            reporterResults.beforeAll
                        );
                    });

                    it('should call begin with url', () => {
                        expect.assertions(3);
                        const calls = [1, 3, 5];
                        for (const [i, call] of calls.entries()) {
                            const results = parseTestResults(
                                consoleSpy.mock.calls[call][0]
                            );
                            expect(results).toStrictEqual(
                                reporterResults.begin[i]
                            );
                        }
                    });

                    it('should call results with results and config', () => {
                        expect.assertions(2);
                        const calls = [2, 4];
                        for (const [i, call] of calls.entries()) {
                            const results = parseTestResults(
                                consoleSpy.mock.calls[call][0]
                            );
                            expect(results).toStrictEqual(
                                reporterResults.results[i]
                            );
                        }
                    });

                    it('should call error with error, url, and config', () => {
                        expect.assertions(1);

                        const results = parseTestResults(
                            consoleSpy.mock.calls[6][0]
                        );
                        expect(results).toStrictEqual(reporterResults.error[0]);
                    });

                    it('should call afterAll with report and config', () => {
                        expect.assertions(1);

                        const results = parseTestResults(
                            consoleSpy.mock.calls[7][0]
                        );
                        expect(results).toStrictEqual(reporterResults.afterAll);
                    });
                }
            );

            describe('without config', () => {
                beforeAll(async () => {
                    consoleSpy = jest
                        .spyOn(console, 'log')
                        .mockImplementation(noop);

                    const runner = createRunner(
                        pa11yciResultsValid,
                        echoReporter,
                        echoReporterOptions
                    );
                    await runner.runAll();
                });

                // BeforeAll is the only function with different behavior without config
                it('should call beforeAll with urls from results if config is not provided', () => {
                    expect.assertions(1);
                    const results = JSON.parse(
                        consoleSpy.mock.calls[0][0]
                    ).urls;
                    expect(results).toStrictEqual(
                        Object.keys(echoReporterResults.afterAll.report.results)
                    );
                });
            });
        });

        describe('runner execution', () => {
            describe('run all', () => {
                let consoleSpy, runner;

                beforeEach(async () => {
                    consoleSpy = jest
                        .spyOn(console, 'log')
                        .mockImplementation(noop);
                    runner = createRunner(
                        pa11yciResultsValid,
                        defaultReporter,
                        defaultReporterOptions,
                        validConfigStringObjectUrl
                    );
                    await runner.runAll();
                });

                it('should call beforeAll', () => {
                    expect.assertions(1);
                    const expectedIndex = 1;
                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        expectedIndex,
                        defaultReporterResults.beforeAll
                    );
                });

                it('should call begin for each url', () => {
                    expect.assertions(3);
                    const calls = [2, 4, 6];
                    for (const [i, call] of calls.entries()) {
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            call,
                            defaultReporterResults.begin[i]
                        );
                    }
                });

                it('should call results for each url without a page error', () => {
                    expect.assertions(2);
                    const calls = [3, 5];
                    for (const [i, call] of calls.entries()) {
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            call,
                            defaultReporterResults.results[i]
                        );
                    }
                });

                it('should call error for each url with a page error', () => {
                    expect.assertions(1);
                    const expectedIndex = 7;
                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        expectedIndex,
                        defaultReporterResults.error[0]
                    );
                });

                it('should call afterAll', () => {
                    expect.assertions(1);
                    const expectedIndex = 8;
                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        expectedIndex,
                        defaultReporterResults.afterAll
                    );
                });

                it('should throw if runAll is called from afterAll without calling reset', async () => {
                    expect.assertions(1);
                    const runnerReset = createRunner(
                        pa11yciResultsValid,
                        mockReporter
                    );
                    await runnerReset.runAll();
                    await expect(runnerReset.runAll()).rejects.toThrow(
                        'runner must be reset'
                    );
                });
            });

            describe('reset', () => {
                const initialState = { state: 'init', url: undefined };

                const runnerStateCases = [
                    {
                        expectedCalls: 3,
                        targetState: RunnerStates.urlResults,
                        testName: 'from an intermediate state'
                    },
                    {
                        expectedCalls: 8,
                        targetState: RunnerStates.afterAll,
                        testName: 'from the final state'
                    }
                ];

                it.each(runnerStateCases)(
                    'should reset to the initial runner state if reset is called $testName',
                    async ({ targetState }) => {
                        expect.assertions(1);
                        const runner = createRunner(
                            pa11yciResultsValid,
                            mockReporter
                        );

                        await runner.runUntil(targetState);
                        await runner.reset();
                        const state = await runner.getCurrentState();

                        await expect(state).toStrictEqual(initialState);
                    }
                );

                it.each(runnerStateCases)(
                    'should re-initialize reporter if reset is called $testName',
                    async ({ targetState, expectedCalls }) => {
                        expect.assertions(2);
                        const consoleSpy = jest
                            .spyOn(console, 'log')
                            .mockImplementation(noop);
                        const runner = createRunner(
                            pa11yciResultsValid,
                            counterReporter
                        );

                        await runner.runUntil(targetState);
                        // Check calls before reset to ensure there is a change
                        expect(consoleSpy).toHaveBeenLastCalledWith(
                            expectedCalls
                        );

                        await runner.reset();
                        await runner.runNext();
                        // After a successful reset, should only have one call
                        expect(consoleSpy).toHaveBeenLastCalledWith(1);
                    }
                );
            });

            describe('run next', () => {
                it('should raise the next event and call the appropriate reporter function', async () => {
                    /* eslint-disable jest/max-expects -- integration test running full sequence */
                    expect.assertions(9);
                    const consoleSpy = jest
                        .spyOn(console, 'log')
                        .mockImplementation(noop);
                    const runner = createRunner(
                        pa11yciResultsValid,
                        defaultReporter,
                        echoReporterOptions,
                        validConfigStringObjectUrl
                    );

                    // Test execute through all states
                    await runner.runNext();
                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        1,
                        defaultReporterResults.beforeAll
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        2,
                        defaultReporterResults.begin[0]
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        3,
                        defaultReporterResults.results[0]
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        4,
                        defaultReporterResults.begin[1]
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        5,
                        defaultReporterResults.results[1]
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        6,

                        defaultReporterResults.begin[2]
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        7,
                        defaultReporterResults.error[0]
                    );

                    await runner.runNext();

                    expect(consoleSpy).toHaveBeenNthCalledWith(
                        8,
                        defaultReporterResults.afterAll
                    );

                    expect(consoleSpy).toHaveBeenCalledTimes(8);

                    /* eslint-enable jest/max-expects -- integration test running full sequence */
                });

                it('should throw if runNext is called from afterAll without calling reset', async () => {
                    expect.assertions(1);
                    const runner = createRunner(
                        pa11yciResultsValid,
                        mockReporter
                    );
                    await runner.runAll();
                    await expect(runner.runNext()).rejects.toThrow(
                        'runner must be reset'
                    );
                });
            });

            // Test cases for runUntil and runUntilNext. Given the commonality
            // in behavior, a common set of tests seems most appropriate.
            const runUntilCases = [
                {
                    describeName: 'run until',
                    functionName: 'runUntil',
                    testNoUrl: { targetState: RunnerStates.beginUrl },
                    testWithUrl: { targetState: RunnerStates.beginUrl }
                },
                {
                    describeName: 'run until next',
                    functionName: 'runUntilNext',
                    testNoUrl: { targetState: RunnerStates.urlResults },
                    testWithUrl: { targetState: RunnerStates.urlResults }
                }
            ];

            describe.each(runUntilCases)(
                '$describeName',

                ({ functionName, testWithUrl, testNoUrl }) => {
                    it('should throw if targetState is not a valid state', async () => {
                        expect.assertions(1);
                        const targetState = 'init';
                        const runner = createRunner(
                            pa11yciResultsValid,
                            mockReporter
                        );
                        await expect(
                            runner[functionName](targetState)
                        ).rejects.toThrow(
                            `targetState "${targetState}" invalid`
                        );
                    });

                    it('should throw if targetState is not found (with no targetUrl)', async () => {
                        expect.assertions(1);
                        const targetState = RunnerStates.urlResults;
                        const runner = createRunner(
                            pa11yciResultsNoUrls,
                            mockReporter
                        );
                        await expect(
                            runner[functionName](targetState)
                        ).rejects.toThrow(
                            `targetState "${targetState}" was not found`
                        );
                    });

                    it('should throw if targetState for targetUrl is not found', async () => {
                        expect.assertions(1);
                        const targetState = RunnerStates.urlResults;
                        const targetUrl = 'http://does.not.exist';
                        const runner = createRunner(
                            pa11yciResultsValid,
                            mockReporter
                        );
                        await expect(
                            runner[functionName](targetState, targetUrl)
                        ).rejects.toThrow(
                            `targetState "${targetState}" for targetUrl "${targetUrl}" was not found`
                        );
                    });

                    it('should raise all events until the target event and URL if URL specified', async () => {
                        expect.assertions(5);
                        const consoleSpy = jest
                            .spyOn(console, 'log')
                            .mockImplementation(noop);
                        const runner = createRunner(
                            pa11yciResultsValid,
                            defaultReporter,
                            echoReporterOptions,
                            validConfigStringObjectUrl
                        );

                        // Test execute through several states, but running the entire results set is not required.
                        await runner[functionName](
                            testWithUrl.targetState,
                            'somewhere-with-issues.html'
                        );

                        expect(consoleSpy).toHaveBeenCalledTimes(4);
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            1,
                            defaultReporterResults.beforeAll
                        );
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            2,
                            defaultReporterResults.begin[0]
                        );
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            3,
                            defaultReporterResults.results[0]
                        );
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            4,
                            defaultReporterResults.begin[1]
                        );
                    });

                    it('should raise all events until the first instance of the target event if no URL specified', async () => {
                        expect.assertions(3);
                        const consoleSpy = jest
                            .spyOn(console, 'log')
                            .mockImplementation(noop);
                        const runner = createRunner(
                            pa11yciResultsValid,
                            defaultReporter,
                            echoReporterOptions,
                            validConfigStringObjectUrl
                        );

                        // Test execute through several states, but running the entire results set is not required.
                        await runner[functionName](testNoUrl.targetState);

                        expect(consoleSpy).toHaveBeenCalledTimes(2);
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            1,
                            defaultReporterResults.beforeAll
                        );
                        expect(consoleSpy).toHaveBeenNthCalledWith(
                            2,
                            defaultReporterResults.begin[0]
                        );
                    });
                }
            );

            describe('errors', () => {
                it('should throw if command is called while another is pending', async () => {
                    expect.assertions(1);
                    const runner = createRunner(
                        pa11yciResultsValid,
                        mockReporter
                    );
                    runner.runNext();
                    await expect(runner.runNext()).rejects.toThrow(
                        'await previous command'
                    );
                });
            });
        });

        describe('runner state', () => {
            let runner;

            beforeAll(() => {
                runner = createRunner(pa11yciResultsValid, mockReporter);
            });

            it('should return the correct state when initialized', async () => {
                expect.assertions(2);
                const expectedCurrentState = { state: 'init', url: undefined };
                const expectedNextState = {
                    state: RunnerStates.beforeAll,
                    url: undefined
                };
                // Call reset to ensure at initial state
                await runner.reset();

                expect(runner.getCurrentState()).toStrictEqual(
                    expectedCurrentState
                );
                expect(runner.getNextState()).toStrictEqual(expectedNextState);
            });

            it('should return the correct state with URL', async () => {
                expect.assertions(2);
                const urls = Object.keys(pa11yciResults.results);
                const expectedCurrentState = {
                    state: RunnerStates.urlResults,
                    url: urls[0]
                };
                const expectedNextState = {
                    state: RunnerStates.beginUrl,
                    url: urls[1]
                };

                // Call reset to ensure at initial state
                await runner.reset();
                await runner.runUntil(
                    expectedCurrentState.state,
                    expectedCurrentState.url
                );

                const currentState = runner.getCurrentState();
                const nextState = runner.getNextState();
                expect(currentState).toStrictEqual(expectedCurrentState);
                expect(nextState).toStrictEqual(expectedNextState);
            });
        });
    });
});
