'use strict';

const { createRunner } = require('../');

describe('pa11y-ci-reporter-runner', () => {
    beforeAll(() => {
        process.env.FORCE_COLOR = 1;
    });

    afterEach(() => {
        jest.restoreAllMocks();
    });

    it('should execute pa11y-ci-reporter-cli-summary with expected results', async () => {
        expect.assertions(1);
        const pa11yciResultsFile =
            './tests/fixtures/results/pa11yci-results-valid.json';
        const reporter = 'pa11y-ci-reporter-cli-summary';
        const runner = createRunner(pa11yciResultsFile, reporter);

        let output = '';
        jest.spyOn(console, 'log').mockImplementation(
            (message) => (output += `${message}\n`)
        );

        await runner.runAll();

        expect(output).toMatchSnapshot();
    });
});
