'use strict';

const path = require('node:path');
const { buildReporter } = require('../lib/reporter-builder');

const defaultReporter = './tests/fixtures/reporters/test-reporter-function.js';

describe('build reporter', () => {
    it('should throw if reporterName is not a string', () => {
        expect.assertions(1);
        const reporterName = {};
        expect(() => buildReporter(reporterName)).toThrow('reporterName');
    });

    it('should throw if reporter cannot be loaded', () => {
        expect.assertions(1);
        const reporterName =
            './tests/fixtures/reporters/reporter-does-not-exist.js';
        expect(() => buildReporter(reporterName)).toThrow(
            'Error loading reporter'
        );
    });

    const testLoadReporter = (reporterName) => {
        expect.assertions(1);
        const reporter = buildReporter(reporterName);
        expect(typeof reporter).toBe('object');
    };

    it('should load reporter that is a dependency by name', () => {
        expect.hasAssertions();
        const reporterName = 'pa11y-ci-reporter-cli-summary';
        testLoadReporter(reporterName);
    });

    it('should load reporter with complete relative path', () => {
        expect.hasAssertions();
        const reporterName = defaultReporter;
        testLoadReporter(reporterName);
    });

    it('should load reporter with incomplete relative path', () => {
        expect.hasAssertions();
        const reporterName =
            'tests/fixtures/reporters/test-reporter-function.js';
        testLoadReporter(reporterName);
    });

    it('should load reporter with absolute path', () => {
        expect.hasAssertions();
        const reporterName = path.resolve(
            process.cwd(),
            'tests/fixtures/reporters/test-reporter-function.js'
        );
        testLoadReporter(reporterName);
    });

    it('should instantiate the reporter if a factory function', () => {
        expect.assertions(1);
        const reporterName = defaultReporter;
        const reporter = buildReporter(reporterName);

        // Reporter is a factory function, so if not instantiated then it
        // will return a function. If instantiated, it will return an object.
        expect(typeof reporter).toBe('object');
    });

    it('should not instantiate the reporter if an object', () => {
        expect.assertions(1);
        const reporterName =
            './tests/fixtures/reporters/test-reporter-object.js';
        const reporter = buildReporter(reporterName);

        // Reporter is am object, so should return an object. If called as
        // a function, will throw and fail.
        expect(typeof reporter).toBe('object');
    });

    describe('reporter events', () => {
        let reporter;

        beforeAll(() => {
            const reporterName = defaultReporter;
            reporter = buildReporter(reporterName);
        });

        it('should return an object with a beforeAll function', () => {
            expect.assertions(1);
            expect(typeof reporter.beforeAll).toBe('function');
        });

        it('should return an object with a begin function', () => {
            expect.assertions(1);
            expect(typeof reporter.begin).toBe('function');
        });

        it('should return an object with a results function', () => {
            expect.assertions(1);
            expect(typeof reporter.results).toBe('function');
        });

        it('should return an object with a error function', () => {
            expect.assertions(1);
            expect(typeof reporter.error).toBe('function');
        });

        it('should return an object with a afterAll function', () => {
            expect.assertions(1);
            expect(typeof reporter.afterAll).toBe('function');
        });
    });
});
